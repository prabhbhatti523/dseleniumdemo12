package seleniumdemo;

import static org.junit.Assert.*;

import java.util.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class AnimalsWebsiteTest {
	
	
	// location of chrome driver file location
	final String CHROMEDRIVER_LOCATION = "/Users/macstudent/Desktop/chromedriver";
	final String URL_TO_TEST = "https://www.webdirectory.com/Animals/";
	
	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		
		//selenium setup
		// Selenium + Chrome
		 		System.setProperty("webdriver.chrome.driver", 
		 				CHROMEDRIVER_LOCATION);
		 		driver = new ChromeDriver();
		 		
		 		// 2. go to the website
		 		driver.get(URL_TO_TEST);
	}

	@After
	public void tearDown() throws Exception {
		
		//close the browser
		driver.close();
	}

	@Test
	public void testNumberOfLinks() {
		// --------
		// 1. Get all the bulleted links in that section
		 		List<WebElement> bulletLinks = driver.findElements(By.cssSelector("table+ul li a"));
		 		
		 		//		// SOLUTION2: Uncomment me to see this code working
		 		//		// -------------------
		 		//		// Get all <ul> elements from the page
		 		//		// Note: There are 2 <ul> elements on the page
		 		//		// First <ul> = section of links that WE WANT
		 		//		// Second <ul> = Jasmine business directory nonsense
		 		//		List<WebElement> uls = driver.findElements(By.cssSelector("ul"));
		 		//		
		 		//		// get the FIRST <ul>
		 		//		WebElement firstUL = uls.get(0);
		 		//		
		 		//		// get all links inside the first <ul>
		 		//		List<WebElement> bulletLinks = firstUL.findElements(By.cssSelector("li a"));
		 				
		 		 		System.out.println("Number of links on page: " + bulletLinks.size());
		 
		 		// 2. Output the links to the screen using System.out.println
		 		// ------
		 		// Iterating through the list of links 
		 		for (int i = 0; i < bulletLinks.size(); i++) {
		 			// Get the current link
		 			WebElement link = bulletLinks.get(i);
		 			
		 			// Get the link text
		 			String linkText = link.getText();
		 
		 			// Get link url
		 			String linkURL = link.getAttribute("href");
		 
		 			// Output it to the screen
		 			System.out.println(linkText + ": " + linkURL);
		 		}
		 		
		 		// 3. Count the number of links in the section
				
				int actualNumLinks = bulletLinks.size();
		 		// 4. Check that the number of links = 10
				
				assertEquals(10, actualNumLinks);
		 
		 		// --------
	}

}
