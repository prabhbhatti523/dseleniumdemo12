package seleniumdemo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class seleniumdemo1 {
	
	
	public static void main(String[] args) throws InterruptedException{
		
		//1. setup selenium + your driver
		// selenium + chrome
		System.setProperty("webdriver.chrome.driver" , "/Users/macstudent/Desktop/chromedriver");
		WebDriver driver = new ChromeDriver();
		
		// 2. go to the website
		driver.get("https://www.seleniumeasy.com/test/basic-first-form-demo.html");
		
		//3. write code to do some stuff on website
		
		//3a. type something into a single input box
		// - get the box
		WebElement inputBox = driver.findElement(By.id("user-message"));
		// type something in box
		inputBox.sendKeys("Hello how are you?");
		
		//3b.  automatically push submit button
		
		// -get the button
		WebElement showMessageButton = driver.findElement(By.cssSelector("form#get-input button"));
		// -push the button
		showMessageButton.click();
		
		//4. close the browser
		Thread.sleep(2000);
        driver.close();		
		
	} 

}
